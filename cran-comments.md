## New version
This is a new version of an R package already on CRAN.


## Test environments

* Windows 11 R 4.4.1
* Win-builder R 4.4.1
* Win-builder R 4.3.3
* Win-builder R-devel
* Rocky Linux R 4.4.1
* MacOS builder R 4.4.0


## R CMD check results

0 errors | 0 warnings | 0 notes


## revdepcheck results

There are no reverse dependencies.